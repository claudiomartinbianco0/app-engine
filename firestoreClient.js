const Firestore = require('@google-cloud/firestore');
const path = require('path');

class FirestoreClient{
    constructor(){
        this.Firestore = new Firestore({
//            projectId: 'devops-model',
//            keyFilename: path.join(__dirname, './devops-model-2889892141b5.json')
        })
    }

    async save(collection, data){
        const docRef = this.Firestore.collection(collection).doc(data.docName);
        await docRef.set(data);        
    }

    async saveSubCollection(rootCol, rootDocName, subCol, subColData){
        const docRef = this.Firestore.collection(rootCol).doc(rootDocName).collection(subCol).doc(subColData.docName);
        await docRef.set(subColData);
    }

    async saveByPath(path2, data){
        const docRef = this.Firestore.doc(path2);
        await docRef.set(data);
    }

    async get(breed){
        const query = this.Firestore.collection('dogs').where('name', '==', breed);
        const querySnapshot = query.get();
        await querySnapshot;
    }    
}

module.exports = new FirestoreClient();