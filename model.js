const Firestore = require('@google-cloud/firestore');

const db = new Firestore({
  projectId: 'devops-model',
});

const Add = async() => {
    await db.collection('cars').add({
        name: 'VW 700',
        colour: 'red'
    });
};

const AddID = async() => {
    await db.collection('cars').doc('fiat').set({
        name: 'Fiat 500',
        colour: 'blue'
    });
};

const GetAll = async() => {
    const carss = await db.collection('cars').get();
    for (const cars of carss.docs){
        console.log(cars.data())
    }   
};

const Get = async() => {
    const query = await db.collection('cars').where('name', '==', 'Fiat 500').get();
    console.log(query.docs[0].data());
};

const Delete = async() => {
    await db.collection('cars').doc('fiat').delete();
};

const Update = async() => {
    await db.collection('cars').doc('fiat').set({
            speed: 'fast'
        }, {merge: true});
};

const Replace = async() => {
    await db.collection('cars').doc('fiat').set({
            name: 'Fiat 500',
            colour: 'red'
        });
};

// Add();
// AddID();
// GetAll();
// Get();
// Delete();
// Update();
Replace();