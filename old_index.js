const express = require('express');
const FirestoreClient = require('./firestoreClient');

const app = express();
app.use(express.json());

app.get('/', (req, res) => {
    res.json({status: 'My test'});
});

app.get('/:breed', async (req, res) => {
const breed = req.params.breed;
const querySnapshot = await FirestoreClient.get(breed);
res.json(querySnapshot.docs[0].data());
});  