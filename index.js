const express = require('express');
const Firestore = require('@google-cloud/firestore');

const db = new Firestore();
const app = express();

app.use(express.json());

app.get('/', (req, res) => {
  res.json({status: 'Hello World! version 4'});
});

app.get('/:breed', async (req, res) => {
  const breed = req.params.breed;
  const query = db.collection('dogs').where('name', '==', breed);
  const querySnapshot = await query.get();
  if (querySnapshot.size > 0) {
    res.json(querySnapshot.docs[0].data());
  }
  else {
    res.json({status: 'Not found!'});
  }
});

app.post('/', async (req, res) => {
  const data = {
    name: req.body.name,
    origin: req.body.origin,
    lifeExpectancy: req.body.lifeExpectancy,
    type: req.body.type
  };
  await db.collection('dogs').doc().set(data);
  res.json({status: 'success', data: { dog: data }});
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});