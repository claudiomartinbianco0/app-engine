const express = require('express');
const Firestore = require('@google-cloud/firestore');

const db = new Firestore();
const app = express();

app.use(express.json());

app.get('/', (req, res) => {
  res.json({status: 'Hello World! version 4'});
});

app.post('/createid',async (req,res)=>{
    let docRef=db.collection('user').doc(req.body.name)
    await docRef.set({
      email: req.body.email,
      password: req.body.password,
    });
     res.json({message:'User created!'});
})

app.post('/create',async (req,res)=>{
    let docRef=db.collection('user')
    await docRef.add({
      email: req.body.email,
      password: req.body.password,
    });
   res.json({message:'User created!'});
})

app.post('/update',async (req,res) => {
    let docRef=db.collection('user').doc(req.body.name)
    await docRef.update({
      email:req.body.email,
      password:req.body.password,
    })
    res.json({message:'User updated!'});
})

app.post('/delete',async (req,res) => {
    await db.collection('user').doc(req.body.name).delete()
    res.json({message:'User deleted!'});
})

app.get('/getall', async (req, res) => {
    let usr=[]
     const users = await db.collection('user').get()
    if (users.docs.length > 0) {
      for (const user of users.docs) {
       usr.push(user.data())
    }}
    res.json(usr)
})

app.post('/get', async (req, res) => {
    let usr=[]
     const users = await db.collection('user').where('email', '==', req.body.email).get()
    if (users.docs.length > 0) {
      for (const user of users.docs) {
       usr.push(user.data())
       console.log(user.data())
       
    }}
    res.json(usr)
})

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});